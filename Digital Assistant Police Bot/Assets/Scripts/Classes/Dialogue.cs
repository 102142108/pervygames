﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogue 
{
    //Parameters -- Core
    public List<string> dialogue = new List<string>(); //The list of text lines the bot will run through in a row when triggered.
    public bool preanim; //Does this dialogue require a preanimation?
    public bool hitsFlag; //Does this dialogue hit a story flag?
    public int flagNum; //If so, which flag? (Refers to CoreHandler's flags[] list)

    //Constructor
    public Dialogue(List<string> dia, bool preanimate, bool flaghit, int flagnumber)
    {
        dialogue = dia;
        preanim = preanimate;
        hitsFlag = flaghit;
        flagNum = flagnumber;
    }
}
