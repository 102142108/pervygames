﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Command 
{
    //Parameters -- Core
    public List<string> cmdLine = new List<string>(); //What does the input string need to contain to elicit the response from the bot? Can be multiple. All lowercased.
    public List<int> cmdFlags = new List<int>(); //How many flags are required for this command to work, if any? Which ones? (From flags[])
    public Dialogue cmdDialogue; //What dialogue will entering this command play?

    //Constructor
    public Command(List<string> possCommands, List<int> reqFlags, Dialogue dialogue)
    {
        cmdLine = possCommands;
        cmdFlags = reqFlags;
        cmdDialogue = dialogue;
    }
}
