﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatSetup : MonoBehaviour
{
    //Parameters -- Core
    public Dialogue curDia; //What dialogue are we running right now?
    public int diaStep;
    public bool diaTyping; //Are we currently typing?
    public float diaDelay; //Delay between one message and the next.
    public float curDelay;

    //Parameters -- GameObjects
    public GameObject locator;
    public GameObject oldTextLocator;
    public GameObject textDestroyerLocator;
    public GameObject textPrefab; //Prefab for the bot chat
    public GameObject curChat;
    public GameObject oldChat;
    public GameObject oldestChat;

    //Parameters -- Bot
    public GameObject botObj;

    // Start is called before the first frame update
    void Start()
    {
        //Set Base Variables
        diaDelay = (1.5f);

        //Start Dialogue
        PlayDialogue(new Dialogue(new List<string> { "Greetings.", "You may be rather confused.", "Please allow me to introduce myself.", "I am D.A.P.D.", "Digital Assistant Police Bot.", "Coloquially, I am referred to as 'Dapper'.", "Please feel free to refer to me as such.", "What can I help you with, Detective?" }, false, false, 0));
    }

    //Update: Check if typing has finished.
    public void Update()
    {
        //Is there a conversation, and it's not typing?
        if (curDia != null && !diaTyping)
        {
            //Tick Up Delay
            curDelay += (0.01f);

            //Have we surpassed the delay timer?
            if (curDelay >= diaDelay)
            {
                //Move on with the next bit of the conversation!
                if ((diaStep + 1) < curDia.dialogue.Count)
                {
                    //Reset Delay
                    curDelay = (0f);

                    //Improve
                    diaStep++;

                    //Type Message
                    StartCoroutine(Type(curDia.dialogue[diaStep]));
                }
                else
                {
                    //Trigger Flags
                    if (curDia.hitsFlag)
                    {
                        //Trigger!
                        GetComponent<CoreHandler>().TriggerFlag(curDia.flagNum);
                    }

                    //End Dialogue
                    curDia = (null);

                    //Reset Delay
                    curDelay = (0f);
                }
            }
        }
        else
        {
            //Reset Delay
            curDelay = (0f);
        }
    }

    //Play Conversation
    public void PlayDialogue(Dialogue newD) 
    {
        //If there's NOT ALREADY a dialogue playing
        if (curDia != null)
        {
            //Do nothing.
            return;
        }

        //Else, we soldier on.
        //Set CurDia
        curDia = (newD);

        //Set Start Phase
        diaStep = (0);

        //Set Typing
        diaTyping = (false);

        //Begin first message
        StartCoroutine(Type(newD.dialogue[0]));

        //Reset Input Field Text
        GetComponent<CoreHandler>().inputObj.GetComponent<Text>().text = ("");
    }

    //Type Message
    public IEnumerator Type(string msg)
    {
        //Debug
        Debug.Log(msg);

        //Set Typing
        diaTyping = (true);

        //Play Talk Anim
        botObj.GetComponent<BotHandler>().botTalking = (true);

        //Create Text Object
        if (oldestChat != null)
        {
            Debug.Log("Nani");
            //Destroy
            Destroy(oldestChat);
            oldestChat = (null);
        }
        if (oldChat != null)
        {
            Debug.Log("Nani");
            //Slide
            oldestChat = (oldChat);
            oldChat = (null);
            StartCoroutine(OldChatSlide());
        }
        if (curChat != null)
        {
            Debug.Log("Nani");
            //Slide
            oldChat = (curChat);
            curChat = (null);
            StartCoroutine(ChatSlide());
        }

        //Instantiate New Chat
        curChat = GameObject.Instantiate(textPrefab, locator.transform);
        curChat.transform.localPosition = (new Vector3(0f, 0f, 0f));
        curChat.GetComponent<Text>().text = ("");

        //Split String
        char[] characters = msg.ToCharArray();

        //Loop
        for (int i = 0; i < msg.Length; i++)
        {
            curChat.GetComponent<Text>().text += characters[i];

            yield return new WaitForSeconds(0.02f);
        }

        //When Finished
        diaTyping = (false);
        curChat.GetComponent<Text>().text = msg;

        //Play Talk Anim
        botObj.GetComponent<BotHandler>().botTalking = (false);
    }

    //Slide Up Old Chat
    public IEnumerator OldChatSlide()
    {
        //Set Opacity/Color
        oldestChat.GetComponent<Text>().color = (new Color(0f, 0f, 0f, 0.1f));

        //Slide up
        float amount = (Vector2.Distance(oldTextLocator.GetComponent<RectTransform>().position, textDestroyerLocator.GetComponent<RectTransform>().position) / 60);

        //GO go go
        for (int i = 0; i < 60; i++)
        {
            //Move
            oldestChat.GetComponent<RectTransform>().position += (new Vector3(0f, amount));

            //Yield
            yield return null;
        }

        //Finish
        oldestChat.GetComponent<RectTransform>().position = textDestroyerLocator.GetComponent<RectTransform>().position;
        oldestChat.GetComponent<Text>().color = (new Color(0f, 0f, 0f, 0f));
    }

    //Slide Up Chat
    public IEnumerator ChatSlide()
    {
        //Set Opacity/Color
        oldChat.GetComponent<Text>().color = (new Color(0f, 0f, 0f, 0.33f));

        //Slide up
        float amount = (Vector2.Distance(locator.GetComponent<RectTransform>().position, oldTextLocator.GetComponent<RectTransform>().position) / 60);

        //GO go go
        for (int i = 0; i < 60; i++)
        {
            //Move
            oldChat.GetComponent<RectTransform>().position += (new Vector3(0f, amount));

            //Yield
            yield return null;
        }

        //Finish
        oldChat.GetComponent<RectTransform>().position = oldTextLocator.GetComponent<RectTransform>().position;
    }
}
