﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoreHandler : MonoBehaviour
{
    //Parameters -- Core
    public List<bool> flags = new List<bool>(); //Story beat flags.
    public List<Command> commands = new List<Command>(); //Inputtable commands.

    //Parameters -- Input
    public string inputString;
    public GameObject inputObj;
    public List<GameObject> clueButtons = new List<GameObject>(); //Inspector
    public List<GameObject> cluePanels = new List<GameObject>();
    public int curClue;

    // Start is called before the first frame update
    void Start()
    {
        //Initialize the Story / Dialogues (Commands)
        InitializeStory();

        //Framerate
        Application.targetFrameRate = (60);

        //Set Starter Flags
        CheckFlags();
    }

    // Update is called once per frame
    void Update()
    {
        //Display Clue
        DisplayClue();
    }

    //Check Flags
    public void CheckFlags()
    {
        //Loop
        for (int i = 0; i < flags.Count; i++)
        {
            //Is Active?
            if (flags[i])
            {
                //Display Button
                clueButtons[i].GetComponent<Image>().color = (new Color(1f, 1f, 1f));

                //Display Text
                clueButtons[i].GetComponent<Transform>().GetChild(0).GetComponent<Text>().color = (new Color(0f, 0f, 0f));
                clueButtons[i].GetComponent<Transform>().GetChild(0).GetComponent<Text>().text = ((i+1).ToString());
            }
            else
            {
                //Display Button
                clueButtons[i].GetComponent<Image>().color = (new Color(.1f, .1f, .1f));

                //Display Text
                clueButtons[i].GetComponent<Transform>().GetChild(0).GetComponent<Text>().color = (new Color(1f, 1f, 1f));
                clueButtons[i].GetComponent<Transform>().GetChild(0).GetComponent<Text>().text = ("---");
            }
        }
    }

    //Click Flag
    public void ClickFlag(int num)
    {
        //Set Targeted
        curClue = num;
    }

    //Display Clue
    public void DisplayClue()
    {
        //Set All to Inactive
        foreach (GameObject obj in cluePanels)
        {
            obj.SetActive(false);
        }

        //Get Current Selected Clue, and Check If It's Been Discovered
        if (flags[curClue])
        {
            //If it has, display it's button as selected
            clueButtons[curClue].GetComponent<Image>().color = (new Color(.5f, 1f, .5f));

            //Set All Others back to Normal
            for (int i = 0; i < flags.Count; i++)
            {
                if (i != curClue)
                {
                    if (flags[i])
                    {
                        clueButtons[i].GetComponent<Image>().color = (new Color(1f, 1f, 1f));
                    }
                    else
                    {
                        clueButtons[i].GetComponent<Image>().color = (new Color(.1f, .1f, .1f));
                    }
                }
            }

            //Display Clue
            cluePanels[curClue].SetActive(true);
        }
        else
        {
            //If it has, display it's button as selected
            clueButtons[curClue].GetComponent<Image>().color = (new Color(.5f, 1f, .5f));

            //Set All Others back to Normal
            for (int i = 0; i < flags.Count; i++)
            {
                if (i != curClue)
                {
                    if (flags[i])
                    {
                        clueButtons[i].GetComponent<Image>().color = (new Color(1f, 1f, 1f));
                    }
                    else
                    {
                        clueButtons[i].GetComponent<Image>().color = (new Color(.1f, .1f, .1f));
                    }
                }
            }

            //Display Clue
            cluePanels[curClue].SetActive(false);
        }
    }

    //Trigger a Flag
    public void TriggerFlag(int flag)
    {
        //Set the Flag to True
        flags[flag] = (true);

        //Check Flags and run their functions
        CheckFlags();
    }

    //Change MEssage
    public void OnChange()
    {
        //Set String
        inputString = inputObj.GetComponent<Text>().text;
    }

    //Send Message
    public void OnPost()
    {
        //Check
        foreach (Command c in commands)
        {
            //Does include?
            foreach (string s in c.cmdLine)
            {
                //Check for inputString
                if (s.ToLower().Contains(inputString.ToLower()) || inputString.ToLower().Contains(s.ToLower()))
                {
                    //Run Command
                    RunCommand(c);

                    //Return
                    return;
                }
            }
        }
    }

    //Run Command
    public void RunCommand(Command cmd)
    {
        //Debug
        Debug.Log("Running command " + cmd.cmdDialogue.dialogue[0]);

        //Check Flags
        if (cmd.cmdFlags.Count > 0)
        {
            //Set Temp
            bool verified = (true);

            //Go through flag requirements
            foreach (int i in cmd.cmdFlags)
            {
                //If any flag that's required isn't true
                if (flags[i] == false)
                {
                    verified = (false);
                }
            }

            //Are we verified?
            if (verified)
            {
                //Debug
                Debug.Log("Verified!");
            }
            else
            {
                //Return
                Debug.Log("Verification error: Flag not hit.");
                return;
            }
        }

        //Run Dialogue
        GetComponent<ChatSetup>().PlayDialogue(cmd.cmdDialogue);
    }

    //Initialize the Story; Create all Dialogues
    public void InitializeStory()
    {
        //"Hello"
        commands.Add(new Command(new List<string> { "Hello ", "Hello,", "Hello.", "Hey ", "Hey,", "Hey.", "Hi ", "Hi,", "Hi.", "Sup ", "Sup?", "What's up ", "What's up?", "Whats up ", "Whats up?", "Yo ", "Yo." }, new List<int>(), new Dialogue(new List<string> { "Hello to you!", "What's good, homie?", "...Just kidding. I would never talk like that.", "I cannot truly speak." }, false, true, 0)));

        //"Prototype"
        commands.Add(new Command(new List<string> { "Prototype ", "Prototype?", "Prototype.", "Prototype" }, new List<int>(), new Dialogue(new List<string> { "A prototype...", "Why yes, I suppose it is.", "My most base of functions are complete.", "Functionality exists for me to emote further.", "I can tilt my head and change colours.", "However these features are not displayed here.", "If you greet me, I can grant you a case file.", "This is how, more than likely, our game will progress.", "Answers to puzzles will be input to me.", "And further information will be returned to you.", "A mutually beneficial arrangement." }, false, false, 0)));
    }
}
