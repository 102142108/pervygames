﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotHandler : MonoBehaviour
{
    //Bot Animation State
    public enum BotStates { idle, confused, angry, nod, shake };
    public BotStates botState;
    public bool botTalking;
    public Animator anim;

    //On Start
    public void Start()
    {
        //Set Animator Controller
        anim = GetComponent<Animator>();
    }

    //Update
    public void Update()
    {
        //Decide on what animation to play
        PlayAnim();
    }

    //Check bot animation state enumerator and play requisite animation (Played by ChatHandler)
    public void PlayAnim()
    {
        //Are we not talking?
        if (!botTalking)
        {
            //Get our current state -- IDLE
            if (botState == BotStates.idle)
            {
                //Play
                anim.Play("Idle");
            }

            //CONFUSED
            else if (botState == BotStates.confused)
            {
                //Play
                anim.Play("Confused");
            }

            //ANGRY
            else if(botState == BotStates.angry)
            {
                //Play
                anim.Play("Angry");
            }

            //NOD
            else if (botState == BotStates.nod)
            {
                //Play
                anim.Play("Nod");
            }

            //SHAKE
            else if (botState == BotStates.shake)
            {
                //Play
                anim.Play("Shake");
            }
        }
        //Are we talking?
        else
        {
            //Get our current state -- IDLE
            if (botState == BotStates.idle)
            {
                //Play
                anim.Play("IdleT");
            }

            //CONFUSED
            else if (botState == BotStates.confused)
            {
                //Play
                anim.Play("ConfusedT");
            }

            //ANGRY
            else if (botState == BotStates.angry)
            {
                //Play
                anim.Play("AngryT");
            }
        }
    }
}
